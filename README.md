# ProjetHSI

# Comment lancer le projet?

    Pour lancer le projet il suffit de lancer dans le terminal la commande : sh bash.sh

# Observation des données via Grafan

    ???

# Question 1

    Pour définir nos données applicatives nous avons créer deux fichier : donnees.csv et type.csv, se trouvant tout deux dans le dossier /bin.
    
# Question 2

    Pour créer la librairie que nous avons nommée : baseCompile.a dans le dossier /src, nous sommes passer par un script python hsi_type_library_generator_in_py_this_time.py également présent dans /src, qui une fois exécuter va générer le .a énnoncé plus tôt.

# Question 3

    Le main de notre application se trouve dans le fichier app.c du répertoire /src.

# Question 4

# Question 5

    Pour cette question les fichiers utilisés sont le Makefile et bash.sh, dans la racine du projet. N'ayant pas réussi à créer de Makefile maitre appelant des sous-makefile nous avons déssider de lancer des commandes suplémentaires dans le fichier bash. C'est à dire que depuis bash.sh nous compilons le fichier python de la question 2 en ligne de comande pour créer le .a puis nous lançons le make qui va exécuter le makefile et enfin nous lançons le driver ainsi que l'exécutable du projet.

# Question 6

# Question 7

# Reste du projet

    Nous nous sommes arrêter aux questions présédentes dut à du temps manquant et des problèmes rencontré qui nous on ralenti dans le développement de l'application.
