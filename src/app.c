/**
* \file app.c
* \brief Main of the BCGV project
* \details 
* \author BOUVART, DOUCET, TESSON
*/

#include <stdio.h>
#include "drv_api.h"
#include "baseCompile.h"
#include <string.h>

#include "fsm_clignotant.h"
#include "fsm_feux.h"
#include "fsm_glace.h"



void envoi_message(u_int16_t mon_message) {
    u_int16_t validateur_message;

    validateur_message = drv_write_udp_20ms(mon_message);
    if (validateur_message != mon_message) {
        printf("Un probleme est arrivé\n");
    }
}

/**
* \brief main function
* \details 
* \param argc number of input arguments
* \param argv value of input arguments here the name of the method
* \return int32_t : Description
*/

int32_t main(int argc, char *argv[]){
    
    int numero_de_trame_attendu = 1;
    init_general();

    while(1) {
        miseAJourDepuisTrame();

        if (get_numb_tram() != numero_de_trame_attendu) {
            printf("Erreur numero trame\n");
        }

        /*
            Algos
            C'est les main1
        */

        envoi_message(encodage_feux_position());
        envoi_message(encodage_feux_croisement());
        envoi_message(encodage_feux_route());
        envoi_message(encodage_feux_clignotant_droit());
        envoi_message(encodage_feux_clignotant_gauche());

       
    }

    u_int8_t s[DRV_UDP_10MS_FRAME_SIZE];
    int32_t drv;
    drv = drv_open();
    printf("la\n");
    while(1)
    {
         int32_t out =   drv_read_udp_10ms(drv, s);

         printf("%u\n", s[0]);
     }

     drv_close(drv);
    return 0;

}