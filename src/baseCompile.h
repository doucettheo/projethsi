#include <stdbool.h> 
#include <stdint.h>

#ifndef STR_LIB_H_
#define STR_LIB_H_

typedef bool bool_t;
typedef uint64_t crc8_t;
typedef uint8_t numb_tram_t;
typedef uint32_t km_t;
typedef uint8_t speed_t;
typedef uint8_t litre_t;
typedef uint32_t revolution_t;
typedef uint8_t bit_carrier_t;
typedef uint8_t operation_state_t;


struct maStructure {
	bool_t  is_warning;
	bool_t  is_position;
	bool_t  is_headlights;
	bool_t  is_road_lights;
	bool_t  is_right_turn;
	bool_t  is_left_turn;
	bool_t  is_wiper;
	bool_t  is_washer;
	crc8_t  crc8;
	numb_tram_t  numb_tram;
	km_t  km;
	speed_t  speed;
	bit_carrier_t  carriage_prob;
	bit_carrier_t  engine_prob;
	litre_t  tank;
	revolution_t  revolution;
	bit_carrier_t  battery_prob;
};

bool_t get_is_warning(void);
void set_is_warning(bool_t ma_variable);

bool_t get_is_position(void);
void set_is_position(bool_t ma_variable);

bool_t get_is_headlights(void);
void set_is_headlights(bool_t ma_variable);

bool_t get_is_road_lights(void);
void set_is_road_lights(bool_t ma_variable);

bool_t get_is_right_turn(void);
void set_is_right_turn(bool_t ma_variable);

bool_t get_is_left_turn(void);
void set_is_left_turn(bool_t ma_variable);

bool_t get_is_wiper(void);
void set_is_wiper(bool_t ma_variable);

bool_t get_is_washer(void);
void set_is_washer(bool_t ma_variable);

crc8_t get_crc8(void);
void set_crc8(crc8_t ma_variable);

numb_tram_t get_numb_tram(void);
void set_numb_tram(numb_tram_t ma_variable);

km_t get_km(void);
void set_km(km_t ma_variable);

speed_t get_speed(void);
void set_speed(speed_t ma_variable);

bit_carrier_t get_carriage_prob(void);
void set_carriage_prob(bit_carrier_t ma_variable);

bit_carrier_t get_engine_prob(void);
void set_engine_prob(bit_carrier_t ma_variable);

litre_t get_tank(void);
void set_tank(litre_t ma_variable);

revolution_t get_revolution(void);
void set_revolution(revolution_t ma_variable);

bit_carrier_t get_battery_prob(void);
void set_battery_prob(bit_carrier_t ma_variable);



void init_general(void);

#endif /* STR_LIB_H_ */