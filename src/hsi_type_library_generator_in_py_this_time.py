nom_et_emplacement_a = "./src/baseCompile.a"
# Fait pour etre execute depuis le Makefile

fichier_ecriture_h = open("./src/baseCompile.h", "w") # Nom du fichier genere
fichier_ecriture_h.write("#include <stdbool.h> \n#include <stdint.h>\n\n#ifndef STR_LIB_H_\n#define STR_LIB_H_\n\n")

fichier_ecriture_c = open("./src/baseCompile.c", "w")
fichier_ecriture_c.write('#include "baseCompile.h"\n')

dictionnaire_variable_type = {}
dictionnaire_type_limitation = {}
chemin_csv = "./bin/"

import csv

with open(chemin_csv+'type.csv') as csv_file:  # Fichier contenant les types
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in csv_reader :
        if (line_count != 0):
            fichier_ecriture_h.write("typedef " +row [2] + " " + row[0] +";\n")
            dictionnaire_type_limitation[row[0]] = row[3]
        line_count = line_count + 1
    csv_file.close()

fichier_ecriture_h.write("\n\nstruct maStructure {\n")

with open(chemin_csv+'donnees.csv') as csv_file_donnees:    
    csv_reader = csv.reader(csv_file_donnees, delimiter=',')
    line_count = 0
    for row in csv_reader :
        if (line_count != 0):
            fichier_ecriture_h.write("\t" +row [2] + "  " + row[1] +";\n")
            dictionnaire_variable_type[row[1]] = row[2]
        line_count = line_count + 1
    csv_file_donnees.close()

fichier_ecriture_h.write("};\n\n")
fichier_ecriture_c.write("\nstatic struct maStructure structure_complete;\n\n")  

for cle in dictionnaire_variable_type:
    # Getters
    fichier_ecriture_h.write(dictionnaire_variable_type[cle]+" get_"+cle+"(void);\n")
    fichier_ecriture_c.write(dictionnaire_variable_type[cle]+" get_"+cle+"(void) {\n\treturn structure_complete."+cle+";\n}\n")
    
    #Aquisition_min_max
    mon_element_type = dictionnaire_type_limitation[dictionnaire_variable_type[cle]]
    mon_element_type = mon_element_type[1:-1]
    separe = mon_element_type.split(";")
    
    min_existe_il = (len(separe) == 2)
    max_existe_il = False
    mon_min = 0
    mon_max = 1
    
    if (dictionnaire_variable_type[cle] == "bool_t"):
        min_existe_il = False
    
    if min_existe_il:
        mon_min = separe[0]
        
        max_existe_il = not(len(separe[1].split("?")) >= 2)
        if max_existe_il:
            split_max = (separe[1].split("^"))
            if len(split_max) == 2:
                mon_max = (str) ( ((int) (split_max[0])) ** ((int) (split_max[1]))-1)
            else:
                mon_max = split_max[0]
    
    
    
    # Setters
    fichier_ecriture_h.write("void set_"+cle+"("+dictionnaire_variable_type[cle]+" ma_variable);\n\n")
    fichier_ecriture_c.write("void set_"+cle+"("+dictionnaire_variable_type[cle]+" ma_variable) {\n")
    if (min_existe_il):
        fichier_ecriture_c.write("\tif (ma_variable < "+mon_min+") {\n\t\tma_variable = "+mon_min+";\n\t}\n")
    if (max_existe_il):
        fichier_ecriture_c.write("\tif (ma_variable > "+mon_max+") {\n\t\tma_variable = "+mon_max+";\n\t}\n")
    
    fichier_ecriture_c.write("\tstructure_complete."+cle+" = ma_variable;\n}\n")
    
    fichier_ecriture_c.write("\n")


#Init
fichier_ecriture_h.write("\n\nvoid init_general(void);\n")
fichier_ecriture_c.write("\n\nvoid init_general(void) {\n")
for cle in dictionnaire_variable_type:
    fichier_ecriture_c.write("\tset_"+cle+"(0);"+ "\n")

fichier_ecriture_c.write("}\n")

fichier_ecriture_h.write("\n#endif /* STR_LIB_H_ */")
fichier_ecriture_h.close()

fichier_ecriture_c.close()



# Compile et nettoie
import os
os.system('gcc -Wall -c ./src/baseCompile.c -o ./src/librairieProto.o')
my_string = 'ar crs ' + nom_et_emplacement_a + ' ./src/librairieProto.o'
# os.system('echo '+my_string)
os.system(my_string)
os.system('rm ./src/baseCompile.c')
os.system('rm ./src/librairieProto.o')
