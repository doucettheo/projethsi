/**
* \file app.c
* \brief Main of the BCGV project
* \details 
* \author BOUVART, DOUCET, TESSON
*/

#include <stdio.h>
#include "drv_api.h"
#include "baseCompile.h"


/**
 * \brief Take lns dataframe and set it's data into our variable
 * \param   structure_lns   Structure to read 
 * \return void
 */
void traitement_lns(lns_frame_t structure_lns) {
    set_is_warning(structure_lns.frame[0] &     0b10000000);
    set_is_position(structure_lns.frame[0] &    0b01000000);
    set_is_headlights(structure_lns.frame[0] &  0b00100000);
    set_is_road_lights(structure_lns.frame[0] & 0b00010000);
    set_is_right_turn(structure_lns.frame[0] &  0b00001000);
    set_is_left_turn(structure_lns.frame[0] &   0b00000100);
    set_is_wiper(structure_lns.frame[0] &       0b00000010);
    set_is_washer(structure_lns.frame[0] &      0b00000001);
    set_crc8(structure_lns.frame[1]);

}

/**
 * \brief Take udp dataframe and set it's data into our variable
 * \param   ma_structure_udp   Structure to read 
 * \return void
 */
void traitement_udp(u_int8_t ma_structure_udp[DRV_UDP_10MS_FRAME_SIZE]) {
    u_int8_t num_tram_temporaire = ma_structure_udp[0];
    if (num_tram_temporaire >= 100) {
        num_tram_temporaire = 1;
    }
    set_numb_tram(num_tram_temporaire);
	set_km( ma_structure_udp[1] + (ma_structure_udp[2]*256) + (ma_structure_udp[3]*65536) + (ma_structure_udp[4]*16777216));
	set_speed(ma_structure_udp[5]);
	set_carriage_prob(ma_structure_udp[6]);
	set_engine_prob(ma_structure_udp[7]);
	set_tank(ma_structure_udp[8]);
	set_revolution( ma_structure_udp[9] + (ma_structure_udp[10]*256) + (ma_structure_udp[11]*65536) + (ma_structure_udp[12]*16777216));
	set_battery_prob(ma_structure_udp[13]);

}

u_int16_t encodage_feux_position() {
    u_int16_t feux_de_position = 0x0100;
    if (get_is_position()) {
        feux_de_position = feux_de_position | 0x0001;
    }
    return feux_de_position;
}

u_int16_t encodage_feux_croisement() {
    u_int16_t feux_de_croisement = 0x0200;
    if (get_is_headlights()) {
        feux_de_croisement = feux_de_croisement | 0x0001;
    }
    return feux_de_croisement;
}

u_int16_t encodage_feux_route() {
    u_int16_t feux_de_route = 0x0300;
    if (get_is_road_lights()) {
        feux_de_route = feux_de_route | 0x0001;
    }
    return feux_de_route;
}

u_int16_t encodage_feux_clignotant_droit() {
    u_int16_t feux_glignotant_droit = 0x0400;
    if (get_is_right_turn()) {
        feux_glignotant_droit = feux_glignotant_droit | 0x0001;
    }
    return feux_glignotant_droit;
}

u_int16_t encodage_feux_clignotant_gauche() {
    u_int16_t feux_glignotant_gauche = 0x0500;
    if (get_is_left_turn()) {
        feux_glignotant_gauche = feux_glignotant_gauche | 0x0001;
    }
    return feux_glignotant_gauche;
}

void miseAJourDepuisTrame(int32_t driver) { // Fonction a executer en threading?
    u_int8_t structure_udp[DRV_UDP_10MS_FRAME_SIZE];
    lns_frame_t structure_lns;
    
    uint32_t lns_data_len;
    lns_data_len = 9;
    int32_t out_trame_udp;
    int32_t out_trame_lns;


    out_trame_udp = drv_read_udp_10ms(driver, structure_udp);
    out_trame_lns = drv_read_lns(driver, & structure_lns , &lns_data_len);

    if (out_trame_udp != DRV_ERROR) {
        traitement_udp(structure_udp);
    }
    if (out_trame_lns != DRV_ERROR) {
        traitement_lns(structure_lns);
    }

    //printf("%d\n",get_is_warning()); 
}

int32_t main(int argc, char *argv[]){
    while (1) {
        int32_t driver;
        driver = drv_open();
        miseAJourDepuisTrame(driver);
        drv_close(driver);
    }
}
