typedef struct {
    int state;
    int event;
    int (*callback)(void);
    int next_state;
} tTransition;

/* States */
#define ST_ANY                             -1
#define ST_INIT                             0
#define ST_OFF                              1
#define ST_WIPER_ON                         2
#define ST_WIPER_WASH_ON                    3
#define ST_TIME_WIPER                       4
#define ST_TERM                           255

#define EV_NONE                             0
#define EV_ANY                             -1
#define EV_CMD_EG_ON_IG_ON                  1
#define EV_CMD_EG_ON_IG_OFF                 2
#define EV_CMD_EG_ON_IG_ON                  3
#define EV_CMD_EG_OFF_IG_OFF                4
#define EV_WIPER_TIME_OFF                   5
#define EV_WIPER_TIME_OFF                   5




static int WiperOff(void){


}

static int WiperOn(void){

}

static int WiperWashOn(void){

}

static int WiperTime(void){

}

/* Transitions */
tTransition trans[] = {
        { ST_OFF, EV_CMD_EG_OFF_IG_OFF, &Off, ST_OFF},
        { ST_OFF, EV_CMD_EG_ON_IG_OFF, &WiperOn, ST_WIPER_ON},

        { ST_WIPER_ON, EV_CMD_EG_ON_IG_OFF,  &WiperOn, ST_WIPER_ON},
        { ST_WIPER_ON, EV_CMD_EG_OFF_IG_OFF, &WiperOff, ST_OFF},
        { ST_WIPER_ON, EV_CMD_EG_ON_IG_ON, &WiperOnWashOn, ST_WIPER_WASH_ON},

        { ST_WIPER_WASH_ON, EV_CMD_EG_ON_IG_ON,  &WiperOnWashon, ST_WIPER_WASH_ON},
        { ST_WIPER_WASH_ON, EV_CMD_EG_ON_IG_OFF, &WiperTime, ST_WIPER_TIME},

        { ST_WIPER_TIME, EV_CMD_EG_ON_IG_ON,  &WiperOnWashon, ST_WIPER_WASH_ON},
        { ST_WIPER_TIME, EV_WIPER_TIME_ON, &WiperTime, ST_WIPER_TIME},
        {ST_WIPER_TIME, EV_WIPER_TIME_OFF, &WiperTime, ST_OFF},


};

#define TRANS_COUNT (sizeof(trans)/sizeof(*trans))

int GetNextEvent(int current_state)
{
    int event = EV_NONE;

    /* Here, you can get the parameters of your FSM */

    /* Build all the events */
    switch (current_state) {
        int cmdeg  = 0;
        int cmdig = 0;
        /* switch case on states */
        case ST_OFF :

            if (cmdeg == 1){
                event = EV_CMD_EG_ON_IG_OFF;
            }
            else{
                if (cmdeg == 0) {
                    event = EV_CMD_EG_OFF_IG_OFF;
                }
            }
            break;

        case ST_WIPER_ON :

            if (cmdeg == 1) {
                if(cmdig== 0){
                    event = EV_CMD_EG_ON_IG_OFF;
                }
                else{
                    if(cmdig== 1){
                        event = EV_CMD_EG_ON_IG_OFF;
                    }
                }

            }

            break;

        case ST_WIPER_WASH_ON :


            if(cmdig== 0){
                event = EV_CMD_EG_ON_IG_OFF;
            }
            else{
                if(cmdig== 1){
                    event = EV_CMD_EG_ON_IG_OFF;
                }
            }
            break;
        case ST_TIME_WIPER :
            int time = 0;
            if(cmdig== 1){
                event = EV_CMD_EG_ON_IG_ON;
            }
            else{
                if(time< 2){
                    event = EV_WIPER_TIME_ON;
                }
                if(time> 2){
                    event = EV_WIPER_TIME_OFF;
                }
            }
            break;
    }
    return event;
}

int Wiper(void)
{
    int i = 0;
    int ret = 0;
    int event = EV_NONE;
    int state = ST_INIT;

    /* While FSM hasn't reach end state */
    while (state != ST_TERM) {

        /* Get event */
        event = GetNextEvent(state);

        /* For each transitions */
        for (i = 0; i < TRANS_COUNT; i++) {
            /* If State is current state OR The transition applies to all states ...*/
            if ((state == trans[i].state) || (ST_ANY == trans[i].state)) {
                /* If event is the transition event OR the event applies to all */
                if ((event == trans[i].event) || (EV_ANY == trans[i].event)) {
                    /* Apply the new state */
                    state = trans[i].next_state;
                    /* Call the state function */
                    ret = (trans[i].callback)();
                    break;
                }
            }
        }
    }

    return 0;
}

