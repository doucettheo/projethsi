/**
* \file app.c
* \brief Main of the BCGV project
* \details 
* \author BOUVART, DOUCET, TESSON
*/

#ifndef TRAME_H_
#define TRAME_H_

#include <stdio.h>
#include "drv_api.h"
#include "baseCompile.a"

static int is_functionning = 1;

/**
 * \brief Take lns dataframe and set it's data into our variable
 * \param   structure_lns   Structure to read 
 * \return void
 */
void traitement_lns(lns_frame_t structure_lns);

/**
 * \brief Take udp dataframe and set it's data into our variable
 * \param   ma_structure_udp   Structure to read 
 * \return void
 */
void traitement_udp(u_int8_t ma_structure_udp[DRV_UDP_10MS_FRAME_SIZE]);

/**
 * \brief Encoding 
 * \param   void
 * \return u_int16_t
 */
u_int16_t encodage_feux_position();

u_int16_t encodage_feux_croisement();

u_int16_t encodage_feux_route();

u_int16_t encodage_feux_clignotant_droit();

u_int16_t encodage_feux_clignotant_gauche();

void miseAJourDepuisTrame();

#endif /* TRAME_H_ */
