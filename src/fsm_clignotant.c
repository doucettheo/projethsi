typedef struct {
    int state;
    int event;
    int (*callback)(void);
    int next_state;
} tTransition;

/* States */
#define ST_ANY                             -1
#define ST_INIT                             0
#define ST_OFF                              1
#define ST_ON_LIGHT_ON                      2
#define ST_ON_LIGHT_OFF                     3
#define ST_ERROR                            4
#define ST_ACQUITTED                        5
#define ST_TERM                           255

#define EV_NONE                             0
#define EV_ANY                             -1
#define EV_CMD_ON_Time                      1
#define EV_CMD_ON_NO_Time                   2
#define EV_CMD_OFF                          3
#define EV_ACQUITAL_ON                      4
#define EV_ACQUITAL_OFF                     5


static int Off(void){


}

static int LightOn(time){
    if (time==1) {
        LightOff()
    }
    else {
        if (time == 0) {
            set_feux(1);
        }
    }
}

static int LightOff(void){
    if (time==1) {
        LightOn()
    }
    else {
        if (time == 0) {
            set_feux(1);
        }
    }
}

static int Error(void){

}

static int Acquitted(void){

}

/* Transitions */
tTransition trans[] = {
        { ST_OFF, EV_CMD_OFF, &Off, ST_OFF},
        { ST_OFF, EV_CMD_ON, &LightOn, ST_ON_LIGHT_ON},

        { ST_ON_LIGHT_ON, EV_CMD_ON_NO_Time, &LightOn, ST_ON_LIGHT_ON},
        { ST_ON_LIGHT_ON, EV_CMD_ON_Time, &LightOff, ST_ON_LIGHT_OFF},
        { ST_ON_LIGHT_ON, EV_CMD_OFF, &Off, ST_OFF},
        { ST_ON_LIGHT_ON, EV_ACQUITAL_OFF, &Error, ST_ERROR},
        { ST_ON_LIGHT_ON, EV_ACQUITAL_ON, &Acquitted, ST_ACQUITTED},

        { ST_ON_LIGHT_OFF, EV_CMD_ON_NO_Time, &LightOff, ST_ON_LIGHT_OFF},
        { ST_ON_LIGHT_OFF, EV_CMD_ON_Time, &LightOn, ST_ON_LIGHT_ON},
        { ST_ON_LIGHT_OFF, EV_CMD_OFF, &Off, ST_OFF},
        { ST_ON_LIGHT_OFF, EV_ACQUITAL_OFF, &Error, ST_ERROR},
        { ST_ON_LIGHT_OFF, EV_ACQUITAL_ON, &Acquitted, ST_ACQUITTED},

        { ST_ACQUITTED, EV_CMD_OFF, &LightOff, ST_OFF},

};

#define TRANS_COUNT (sizeof(trans)/sizeof(*trans))

int GetNextEvent(int current_state)
{
    int event = EV_NONE;

    /* Here, you can get the parameters of your FSM */

    /* Build all the events */
    switch (current_state) {
        int cmd  = 0;
        int acq = 0;
        int time =0;
        /* switch case on states */
        case ST_OFF :

            if (cmd == 1){
                event = EV_CMD_ON;
            }
            else{
                if (cmd == 0) {
                    event = EV_CMD_OFF;
                }
            }
            break;

        case ST_ON_LIGHT_ON :

            if (cmd == 1) {
                if(acq== 0){
                    event = EV_ACQUITAL_OFF;
                }
                else{
                    if(acq== 1){

                        event = EV_ACQUITAL_ON;

                    }
                }

            }
            else{
                if (cmd == 0) {
                    event = EV_CMD_OFF;
                }
            }


            break;

        case ST_ACQUITTED :

            if (cmd == 1){
                event = EV_CMD_ON;
            }
            else{
                if (cmd == 0) {
                        event = EV_CMD_OFF;
                    }

                }
            }
            break;
        case ST_ERROR :
            printf("error");

            break;
    }
    return event;
}

int Warning(void)
{
    int i = 0;
    int ret = 0;
    int event = EV_NONE;
    int state = ST_INIT;

    /* While FSM hasn't reach end state */
    while (state != ST_TERM) {

        /* Get event */
        event = GetNextEvent(state);

        /* For each transitions */
        for (i = 0; i < TRANS_COUNT; i++) {
            /* If State is current state OR The transition applies to all states ...*/
            if ((state == trans[i].state) || (ST_ANY == trans[i].state)) {
                /* If event is the transition event OR the event applies to all */
                if ((event == trans[i].event) || (EV_ANY == trans[i].event)) {
                    /* Apply the new state */
                    state = trans[i].next_state;
                    /* Call the state function */
                    ret = (trans[i].callback)();
                    break;
                }
            }
        }
    }

    return 0;
}
