typedef struct {
    int state;
    int event;
    int (*callback)(void);
    int next_state;
} tTransition;

/* States */
#define ST_ANY                             -1
#define ST_INIT                             0
#define ST_LIGHT_OFF                        1
#define ST_LIGHT_ON                         2
#define ST_ERROR                            3
#define ST_ACQUITTED                        4
#define ST_TERM                           255

#define EV_NONE                             0
#define EV_ANY                             -1
#define EV_CMD_ON                           1
#define EV_CMD_OFF                          2
#define EV_ACQUITAL_ON                      3
#define EV_ACQUITAL_OFF                     4


static int LightOff(Light){

    switch(light){
        case 0 :
            set_feux_pos(0);
        case 1 :
            set_feux_crois(0);
        case 2 :
            set_feux_rout(0);
    }
}

static int LightOn(Light){
    switch(light){
        case 0 :
            set_feux_pos(1);
        case 1 :
            set_feux_crois(1);
        case 2 :
            set_feux_rout(1);
    }
}

static int Error(void){

}

static int Acquitted(void){

}

/* Transitions */
tTransition trans[] = {
        { ST_LIGHT_OFF, EV_CMD_OFF, &LightOff, ST_LIGHT_OFF},
        { ST_LIGHT_OFF, EV_CMD_ON, &LightOn, ST_LIGHT_ON},

        { ST_LIGHT_ON, EV_CMD_ON, &LightOn, ST_LIGHT_ON},
        { ST_LIGHT_ON, EV_CMD_OFF, &LightOff, ST_LIGHT_OFF},
        { ST_LIGHT_ON, EV_ACQUITAL_OFF, &Error, ST_ERROR},
        { ST_LIGHT_ON, EV_ACQUITAL_ON, &Acquitted, ST_ACQUITTED},

        { ST_ACQUITTED, EV_CMD_OFF, &LightOff, ST_LIGHT_OFF},
        { ST_ACQUITTED, EV_CMD_ON, &Acquitted, ST_ACQUITTED},
};

#define TRANS_COUNT (sizeof(trans)/sizeof(*trans))

int GetNextEvent(int current_state)
{
    int event = EV_NONE;

    /* Here, you can get the parameters of your FSM */

    /* Build all the events */
    switch (current_state) {
        int cmd  = 0;
        int acq = 0;
        /* switch case on states */
        case ST_LIGHT_OFF :

            if (cmd == 1){
                event = EV_CMD_ON;
            }
            else{
                if (cmd == 0) {
                    event = EV_CMD_OFF;
                }
            }
            break;

        case ST_LIGHT_ON :

            if (cmd == 1) {
                if(acq== 0){
                    event = EV_ACQUITAL_OFF;
                }
                else{
                    if(acq== 1){
                        event = EV_ACQUITAL_ON;
                    }
                }

            }
            else{
                if (cmd == 0) {
                    event = EV_CMD_OFF;
                }
            }


            break;

        case ST_ACQUITTED :

            if (cmd == 1){
                event = EV_CMD_ON;
            }
            else{
                if (cmd == 0) {
                    event = EV_CMD_OFF;
                }
            }
            break;
        case ST_ERROR :
            printf("error");

            break;

    return event;
    }
    return event;
}

int Light(void)
{
    int i = 0;
    int ret = 0;
    int event = EV_NONE;
    int state = ST_INIT;

    /* While FSM hasn't reach end state */
    while (state != ST_TERM) {

        /* Get event */
        event = GetNextEvent(state);

        /* For each transitions */
        for (i = 0; i < TRANS_COUNT; i++) {
            /* If State is current state OR The transition applies to all states ...*/
            if ((state == trans[i].state) || (ST_ANY == trans[i].state)) {
                /* If event is the transition event OR the event applies to all */
                if ((event == trans[i].event) || (EV_ANY == trans[i].event)) {
                    /* Apply the new state */
                    state = trans[i].next_state;
                    /* Call the state function */
                    ret = (trans[i].callback)();
                    break;
                }
            }
        }
    }

    return 0;
}
