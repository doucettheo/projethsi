CC := gcc
CFLAGS := -W -Wall -pedantic
LDFLAGS :=
BINDIR := bin
SRCDIR := src
DOCDIR := docs
DOXYFILE := Doxyfile
LDLIBS := $(wildcard $(SRCDIR)/*.a)

SRC := $(wildcard $(SRCDIR)/*.c)
OBJS := $(subst $(SRCDIR), $(BINDIR),$(SRC:.c=.o))
EXEC ?= Projet
BIN := $(EXEC)
 
.PHONY: all
all: $(BIN)

$(BINDIR)/%.o: $(SRCDIR)/%.c 
	$(CC) $(CFLAGS) -c $< -o $@

$(BIN): $(OBJS)
	$(CC) $^ -o $@ $(LDLIBS)


.PHONY: doc
doc: $(DOXYFILE)
	@doxygen $(DOXYFILE)

.PHONY: clean
clean:
	@rm -rf $(SRCDIR)/*.o

.PHONY: mrproper
mrproper: clean
	@rm -rf $(BIN) $(DOCDIR)/html $(DOCDIR)/latex

.PHONY: help
help:
	@echo 'General targets: '
	@echo ' help	  - display this help'
	@echo 'Building targets: '
	@echo ' all 	  - build the project'
	@echo ' doc 	  - build documentation'
	@echo 'Cleaning targets: '
	@echo ' clean    - remove compilation objects'
	@echo ' mrproper - remove anything that can be generated'
